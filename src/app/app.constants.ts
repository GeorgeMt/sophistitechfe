import { HttpHeaders } from '@angular/common/http';

export const SPIN_PRODUCT_TIME = 390;
export const CAPTURE_INTERVAL = 5000;
export const GET_RESULTS_INTERVAL = 10000;
export const SERVER_URL = 'http://10.20.1.173:8081/visionApi';

export const SEND_IMAGE_URL = SERVER_URL + '/image';
export const RESET_URL = SERVER_URL + '/reset';
export const GET_RESULTS_URL = SERVER_URL + '/products';

export const IMAGE_CHANGE_THRESHOLD = 70;


export const HttpDefaultHeaders = new HttpHeaders({
  })

export const HttpDefaultOptions = {
    headers: HttpDefaultHeaders,
    'cache': false,
    'Cache-Control': 'no-cache',
    'Pragma': 'no-cache',
    'If-Modified-Since': '0',
  };

export const TEST_PRODUCTS = [
 {
   "id": 1,
   "name": "Tommy Hilfiger Lightweight Hooded Jacket",
   "gender": "boy",
   "labels": ["red", "child", "jacket"],
   "imageURL": "https://shoptommy.scene7.com/is/image/ShopTommy/KB04321_627_FNT?wid=455&hei=455&fmt=jpeg&qlt=90%2C0&op_sharpen=1&resMode=trilin&op_usm=0.8%2C1.0%2C6%2C0&iccEmbed=0",
   "url": "https://usa.tommy.com/en/sale/tommy-hilfiger-children-kids-boys-sale/th-kids-lightweight-hooded-jacket-kb04321",
   "references": [
     {
       "reference": "KidsFashion",
       "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
     },
     {
       "reference": "WomenStreetwear",
       "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
     },
     {
       "reference": "ZaraSummer",
       "url": "https://www.zara.com/us/en/kids-l1.html"
     },
     {
       "reference": "HoodieWear",
       "url": "https://www.vogue.com/projects/13399072/ways-to-wear-a-hoodie/"
     }
   ]
 },
 {
   "id": 2,
   "name": "H&M Padded bomber jacket",
   "gender": "boy",
   "labels": ["red", "child", "jacket"],
   "imageURL": "https://lp2.hm.com/hmgoepprod?set=source[/0f/bc/0fbcb1ed3ee9d361fa95deb6f8c685614aa9a50a.jpg],origin[dam],category[kids_babygirl_outdoor],type[DESCRIPTIVESTILLLIFE],res[s],hmver[1]&call=url[file:/product/main]",
   "url": "https://www2.hm.com/en_cn/productpage.0571479002.html",
   "references": [
     {
       "reference": "KidsFashion",
       "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
     },
     {
       "reference": "WomenStreetwear",
       "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
     }
   ]
 },
 {
   "id": 3,
   "name": "Kids Varsity College Jacket",
   "gender": "boy",
   "labels": ["red", "child", "jacket"],
   "imageURL": "https://images-na.ssl-images-amazon.com/images/I/71f40OtBR0L._UX385_.jpg",
   "url": "https://www.amazon.co.uk/COLLEGE-JACKET-American-Letterman-Baseball/dp/B00CUBHJL0",
   "references": [
     {
       "reference": "KidsFashion",
       "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
     },
     {
       "reference": "WomenStreetwear",
       "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
     }
   ]
 },
 {
   "id": 4,
   "name": "Tapered Faux Leather Trousers",
   "gender": "female",
   "labels": ["female", "black", "leather", "pants"],
   "imageURL": "https://images.asos-media.com/products/asos-design-tapered-leather-look-pants/10266252-4?$XXL$&wid=513&fit=constrain",
   "url": "https://www.asos.com/asos-design/asos-design-tapered-leather-look-trousers/prd/10266252?clr=black&SearchQuery=&cid=17934&gridcolumn=3&gridrow=3&gridsize=4&pge=1&pgesize=72&totalstyles=32",
   "references": [
     {
       "reference": "KidsFashion",
       "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
     },
     {
       "reference": "WomenStreetwear",
       "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
     }
   ]
 },
 {
   "id": 5,
   "name": "Faux Leather Leggings with side stripes",
   "gender": "female",
   "labels": ["female", "black", "leather", "pants"],
   "imageURL": "https://static.zara.net/photos///2018/S/0/1/p/3427/100/800/2/c-215-0-2048-3072/w/560/3427100800_2_6_1.jpg?ts=1530802882855",
   "url": "https://www.zara.com/nz/en/faux-leather-leggings-with-side-stripes-p03427100.html",
   "references": [
     {
       "reference": "KidsFashion",
       "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
     },
     {
       "reference": "WomenStreetwear",
       "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
     }
   ]
 },
 {
   "id": 6,
   "name": "Coloured Suit Blazer",
   "gender": "male",
   "labels": ["gentleman", "coloured", "suit", "jacket"],
   "imageURL": "https://static.zara.net/photos///2019/V/0/2/p/4318/518/620/2/w/560/4318518620_2_4_1.jpg?ts=1551981440147",
   "url": "https://www.zara.com/uk/en/coloured-suit-blazer-p04318518.html",
   "references": [
     {
       "reference": "KidsFashion",
       "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
     },
     {
       "reference": "WomenStreetwear",
       "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
     }
   ]
 },
 {
  "id": 7,
   "name": "Comfort Knit Suit Blazer",
   "gender": "male",
   "labels": ["gentleman", "navy", "suit", "jacket"],
   "imageURL": "https://static.zara.net/photos///2019/V/0/2/p/0706/608/401/3/w/560/0706608401_2_1_1.jpg?ts=1549551779783",
   "url": "https://www.zara.com/za/en/4-way-check-comfort-knit-suit-blazer-p00706608.html?v1=8289574&v2=1009549",
   "references": [
     {
       "reference": "KidsFashion",
       "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
     },
     {
       "reference": "WomenStreetwear",
       "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
     }
   ]
 },
 {
   "id": 8,
   "name": "Basic Blazer",
   "gender": "male",
   "labels": ["male", "navy", "suit", "jacket"],
   "imageURL": "https://static.zara.net/photos///2019/W/0/2/p/1608/420/401/2/w/560/1608420401_2_1_1.jpg?ts=1548868604311",
   "url": "https://www.zara.com/za/en/basic-blazer-p08574310.html?v1=8548129&v2=1009549",
   "references": [
     {
       "reference": "KidsFashion",
       "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
     },
     {
       "reference": "WomenStreetwear",
       "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
     }
   ]
 },
 {
   "id": 9,
   "name": "Mom Grey Jeans",
   "gender": "female",
   "labels": ["female", "black", "pants"],
   "imageURL": "https://images.asos-media.com/products/pimkie-mom-jeans-in-grey/11413440-1-grey?$XXL$&wid=513&fit=constrain",
   "url": "https://www.asos.com/pimkie/pimkie-mom-jeans-in-grey/prd/11413440?CTAref=We%20Recommend%20Carousel_11&featureref1=we%20recommend%20pers",
   "references": [
     {
       "reference": "KidsFashion",
       "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
     },
     {
       "reference": "WomenStreetwear",
       "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
     },
     {
       "reference": "SuitTrends2019",
       "url": "https://www.peoplemagazine.co.za/fashion-and-beauty/shop-these-suit-trends-2019/"
     }
   ]
 }
];

export const TEST_REFERENCES = [
 {
   "reference": "KidsFashion",
   "url": "https://www.ubmfashion.com/blogs/childrens-springsummer-2019-trend-report-biotanica-glen-park"
 },
 {
   "reference": "WomenStreetwear",
   "url": "https://www.vogue.com/article/spring-2019-street-style-trends"
 },
 {
   "reference": "ZaraSummer",
   "url": "https://www.zara.com/us/en/kids-l1.html"
 },
 {
   "reference": "HoodieWear",
   "url": "https://www.vogue.com/projects/13399072/ways-to-wear-a-hoodie/"
 },
 {
   "reference": "WearSuitJacket",
   "url": "https://theidleman.com/blogs/style/wear-suit-jacket-men"
 },
 {
   "reference": "SuitTrends2019",
   "url": "https://www.peoplemagazine.co.za/fashion-and-beauty/shop-these-suit-trends-2019/"
 }
];
