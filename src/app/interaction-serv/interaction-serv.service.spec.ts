import { TestBed } from '@angular/core/testing';

import { InteractionServService } from './interaction-serv.service';

describe('InteractionServService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InteractionServService = TestBed.get(InteractionServService);
    expect(service).toBeTruthy();
  });
});
