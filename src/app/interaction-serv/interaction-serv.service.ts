import { Injectable } from '@angular/core';
import * as Rx from 'rxjs';
import * as uuid from 'uuid';
import { HttpClient } from '@angular/common/http';

import { Product } from '../models/product';
import { ResetRequest } from '../models/resetRequest';
import { ResultsRequest } from '../models/resultsRequest';
import { SEND_IMAGE_URL,RESET_URL,GET_RESULTS_URL, TEST_PRODUCTS, TEST_REFERENCES, HttpDefaultOptions } from '../app.constants';


@Injectable({
  providedIn: 'root'
})
export class InteractionServService {

  private _results: Rx.BehaviorSubject<Product[]> = new Rx.BehaviorSubject([]);

  public readonly results: Rx.Observable<Product[]> = this._results.asObservable();

  private _labels: Rx.BehaviorSubject<string> = new Rx.BehaviorSubject('');

  public readonly labels: Rx.Observable<string> = this._labels.asObservable();

  private _id: string;

  public adult: string;
  public gender: string;

  constructor(private _http: HttpClient) {
    this._id = uuid.v4(); //the initial session id
    console.log('Interaction service spawned with session ' + this._id);
    this.adult = 'child';
    this.gender = 'male';
  }

  sendImage(blob: Blob) {
    console.log('Sending the blob for session ' + this._id);

    let formData = new FormData();

    formData.append('file', blob, 'img'+this._id);
    //formData.append('sessionId', this._id);

    let computedGender = '';

    if (this.gender == 'male' ) {
      if (this.adult == 'adult') {
        computedGender = 'male';
      } else {
        computedGender = 'boy';
      }
    } else {
      if (this.adult == 'adult') {
        computedGender = 'female';
      } else {
        computedGender = 'girl';
      }
    }

    //formData.append('gender', computedGender);

    this._http.post<string>(SEND_IMAGE_URL + '?sessionId=' + this._id + '&gender='+computedGender, formData, HttpDefaultOptions).subscribe(
      data => {
        console.log('Image sent');
        this._labels.next(data);
        console.log(data);
      },
      error => {
          console.log('Error');
          console.log(error.error.text);
          this._labels.next(error.error.text);
        //alert(JSON.stringify(error));
        //console.log(error);
      }
    )

  }

  getResults() {
    //let resultsRequest = new FormData();
    //resultsRequest.append("sessionId", this._id);

    var promise = this._http.get<Product[]>(GET_RESULTS_URL + '?sessionId=' + this._id, HttpDefaultOptions);

    promise.subscribe(
      data => {
        this._results.next(data);
      },
      error => {
        //alert(JSON.stringify(error));
        //console.log(error);
        let res = [];
        this._results.next(res);
      }
    );
    return promise;
  }

  resetSession(isFirst: boolean) {
    let oldSessionId = this._id;
    this._id = uuid.v4();
    console.log('New session ' + this._id);
    let resetRequest = new FormData();
    resetRequest.append("sessionId", oldSessionId);

    let mes = 'Goodbye!';
    if (isFirst) {
      mes = 'Hello!';
    }

    var msg = new SpeechSynthesisUtterance(mes);

    window.speechSynthesis.getVoices().forEach(function(voice) {
      //console.log(voice.name, voice.default ? voice.default :'');
      if (voice.name == 'Microsoft Zira Desktop - English (United States)') {
        msg.voice = voice;
      }
    });
    window.speechSynthesis.speak(msg);
    console.log(mes);

    this._http.get(RESET_URL + '?sessionId=' + oldSessionId, HttpDefaultOptions).subscribe(
      data => {
        console.log('Reset request sent');
      },
      error => {
        //alert(JSON.stringify(error));
        //console.log(error);
      }
    );
  }


}
