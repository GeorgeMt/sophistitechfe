import { Component, OnInit, Input } from '@angular/core';
import { InteractionServService } from '../interaction-serv/interaction-serv.service';
import { SPIN_PRODUCT_TIME } from '../app.constants';

@Component({
    selector: 'app-gender-checkbox',
    templateUrl: './gender-checkbox.component.html',
    styleUrls: ['./gender-checkbox.component.scss']
})
export class GenderCheckboxComponent implements OnInit {

    @Input() _checkedValue: string;
    @Input() _uncheckedValue: string;
    @Input() isAgeStage: boolean = false;
    @Input() type: string = 'gender';

    icon_type: string = 'child_care';
    isChecked: boolean;

    constructor(private _InteractionServService: InteractionServService) { }

    ngOnInit() {

    }

    checkValue(checkbox): void {
        console.log('TCL: GenderCheckboxComponent -> constructor -> checkbox', checkbox, this.type)
        if (this.type === 'age') {
            console.log('TCL: GenderCheckboxComponent -> constructor -> checkbox.checked', checkbox.checked)
            if (checkbox.checked) {

                this.icon_type = 'smoking_room'
            } else {
                this.icon_type = 'child_care'
                console.log('TCL: GenderCheckboxComponent -> constructor -> this.icon_type', this.icon_type)
            }
        }
        // var path = document.getElementById('path');
        // var bow = document.getElementById('bow');
        // var male = document.getElementById('male');

        let mes = '';
        if (this.type === 'age') {
            if (checkbox.checked) {
                mes = 'adult';
            } else {
                mes = 'child';
            }
            console.log(mes);
            var msg = new SpeechSynthesisUtterance(mes);

            window.speechSynthesis.getVoices().forEach(function (voice) {
                //console.log(voice.name, voice.default ? voice.default :'');
                if (voice.name == 'Microsoft Zira Desktop - English (United States)') {
                    msg.voice = voice;
                }
            });
            window.speechSynthesis.speak(msg);
            this._InteractionServService.adult = checkbox.checked ? 'adult' : 'child';
        } else {
          let mes = '';
            if (checkbox.checked) {
                mes = 'female';
            } else {
                mes = 'male';
            }
            console.log(mes);
            var msg = new SpeechSynthesisUtterance(mes);

            window.speechSynthesis.getVoices().forEach(function (voice) {
                //console.log(voice.name, voice.default ? voice.default :'');
                if (voice.name == 'Microsoft Zira Desktop - English (United States)') {
                    msg.voice = voice;
                }
            });
            window.speechSynthesis.speak(msg);
            this._InteractionServService.gender = checkbox.checked ? 'female' : 'male';
        }
        // if (checkbox.checked) {
        //     male.classList.remove('ma');
        //     setTimeout(function () {
        //         path.classList.add('fe');
        //         male.classList.add('fe');
        //         bow.classList.add('fe');
        //     }, SPIN_PRODUCT_TIME);
        // }
        // else {
        //     male.classList.add('ma');
        //     setTimeout(function () {
        //         path.classList.remove('fe');
        //         male.classList.remove('fe');
        //         bow.classList.remove('fe');
        //     }, SPIN_PRODUCT_TIME);
        // }
    }
}
