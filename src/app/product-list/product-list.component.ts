import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product';
import { InteractionServService } from '../interaction-serv/interaction-serv.service';
import { SPIN_PRODUCT_TIME } from '../app.constants';

import _ from "lodash";

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
    products: Product[];
    labels: string;
    openWindow: any;

    constructor(private _InteractionServService: InteractionServService) { }

    ngOnInit() {

        this.products = [];
        this.labels = '';
        this._InteractionServService.results.subscribe((data) => {
            this.products = data;
            for (var i =0; i < this.products.length; i++) {
              this.products[i].id = i;
            }
        });
        this._InteractionServService.labels.subscribe((data) => {
          this.labels = data;
          console.log(this.labels);
        })

    }

    toggleCard(product): void {
        product.toggled = !product.toggled;

        var msg = new SpeechSynthesisUtterance(product.name);

        window.speechSynthesis.getVoices().forEach(function(voice) {
          //console.log(voice.name, voice.default ? voice.default :'');
          if (voice.name == 'Microsoft Zira Desktop - English (United States)') {
            msg.voice = voice;
          }
        });
        window.speechSynthesis.speak(msg);


        setTimeout(function () {
            product.showLables = product.toggled;
        }, SPIN_PRODUCT_TIME);

        var el = document.getElementById('cf_' + product.id);
        if (el.classList.contains("card--front--flip"))
            el.classList.remove("card--front--flip");
        else
            el.classList.add("card--front--flip");

        el = document.getElementById('cb_' + product.id);
        if (el.classList.contains("card--back--flip"))
            el.classList.remove("card--back--flip");
        else
            el.classList.add("card--back--flip");
    }

    showOverlay(event, product): void {
        console.log('TCL: ProductListComponent -> constructor -> product', product)

        var msg = new SpeechSynthesisUtterance(product.name);

        window.speechSynthesis.getVoices().forEach(function(voice) {
          //console.log(voice.name, voice.default ? voice.default :'');
          if (voice.name == 'Microsoft Zira Desktop - English (United States)') {
            msg.voice = voice;
          }
        });
        window.speechSynthesis.speak(msg);

        event.stopPropagation();
        this.openSimulationWindow(product.url);
    }
    openSimulationWindow(url: string) {
        if (this.openWindow) {
            this.openWindow.close();
        }
        var xVal = window.screen.width - 450;
        var yVal = window.screen.height - 350;
        this.openWindow = window.open('' + url + '',
            'product', 'top=130,left=50,height=' + yVal + ',width=' + xVal + ',status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,dialog=yes,resizable=no');
    }

    goToReference(referenceUrl): void {
        if (this.openWindow) {
            this.openWindow.close();
        }
        var xVal = window.screen.width - 450;
        var yVal = window.screen.height - 350;
        this.openWindow = window.open('' + referenceUrl + '',
            'product', 'top=130,left=50,height=' + yVal + ',width=' + xVal + ',status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,dialog=yes,resizable=no');
    }
}
