import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { InteractionServService } from '../interaction-serv/interaction-serv.service'
import { CAPTURE_INTERVAL, GET_RESULTS_INTERVAL, IMAGE_CHANGE_THRESHOLD } from '../app.constants';


@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.scss']
})
export class CameraComponent implements OnInit {
  videoplayer: HTMLVideoElement;

  @ViewChild('video')
    set mainVideoEl(el: ElementRef) {
    this.videoplayer = el.nativeElement;
  }

  @ViewChild('upload') uploadField;
  @ViewChild('preview') previewImage;
  @ViewChild('fileInput') fileInput;

  canvas: any;
  context: any;

  isLive: boolean = true;
  isFirst: boolean = true;
  doFlash: boolean = false;
  help:  boolean = false;

  constructor(private _interactionService: InteractionServService) { }

  ngOnInit() {
    console.log(this.videoplayer);
    console.log(CAPTURE_INTERVAL);

    let video: HTMLVideoElement = this.videoplayer;

    this.canvas = window.document.createElement('canvas');
    this.canvas.height = 480;
    this.canvas.width = 640;
    this.context = this.canvas.getContext('2d');

    if(window.navigator.mediaDevices && window.navigator.mediaDevices.getUserMedia) {
      window.navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
          //video.src = window.URL.createObjectURL(stream);
          video.srcObject = stream;
          video.play();
          console.log(video);
      });
    }

    this.initImageCapture();

    this.initResultsGetter();

    this._interactionService.results.subscribe(function(data) {
      console.log('Received a new event!');
      console.log(data);
    });
  }

  initImageCapture() {
    //console.log('Action!');
      //let video: HTMLVideoElement = this.videoplayer;

      /*
      setInterval(function(that) {
        // console.log('say cheese!');
        if (that.isLive) {
          let prevImg = that.context.getImageData(0, 0, that.canvas.width, that.canvas.height);
          that.context.drawImage(video, 0, 0);
          let currentImg = that.context.getImageData(0, 0, that.canvas.width, that.canvas.height);

          if (that.rmsDiff(prevImg.data, currentImg.data) > IMAGE_CHANGE_THRESHOLD) {
            console.log('||||||||||||||||||||||||||||||||||||||||||||');
            console.log('|||||||||||||||CHANGE OCCURED|||||||||||||||');
            console.log('||||||||||||||||||||||||||||||||||||||||||||');

            //here, send the reset

            that._interactionService.resetSession(that.isFirst);
            that.isFirst = !that.isFirst;
          }


          that.canvas.toBlob(function(blob){
            //console.log(blob);
            //var fileURL = URL.createObjectURL(blob);
            that._interactionService.sendImage(blob);
          }, 'image/jpeg', 0.95);

        }
      }, CAPTURE_INTERVAL, this);
      */
  }

  makePic() {

    this.doFlash = true;
    setTimeout(function () {
        this.doFlash = false;
    }.bind(this), 600);

    let video: HTMLVideoElement = this.videoplayer;
    if (this.isLive) {
      this.context.drawImage(video, 0, 0);
      let that = this;
      this.canvas.toBlob(function(blob){
        //console.log(blob);
        //var fileURL = URL.createObjectURL(blob);
        that._interactionService.sendImage(blob);
      }, 'image/jpeg', 0.95);
    } else {
      this.resetSession();
    }
  }

  initResultsGetter() {
      this.findProducts();
  }

  findProducts(): void {
    this._interactionService.getResults().subscribe(() => {
        setTimeout(function(that) {
            that.findProducts();
        }, GET_RESULTS_INTERVAL, this);
    }, () => {
        setTimeout(function(that) {
          that.findProducts();
        }, GET_RESULTS_INTERVAL, this);
    })
  }

  resetSession() {
    this.isLive = true;
    this.isFirst = true;
    let video: HTMLVideoElement = this.videoplayer;

    video.play();

    console.log('Reset session');
    this._interactionService.resetSession(this.isFirst);
  }

  rmsDiff(data1,data2){
      var squares = 0;
      for(var i = 0; i<data1.length; i++){
          squares += (data1[i]-data2[i])*(data1[i]-data2[i]);
      }
      var rms = Math.sqrt(squares/data1.length);
      console.log(rms);
      return rms;
  }

  uploadFile() {
    console.log('Upload');
    console.log(this.uploadField);
    this.uploadField.nativeElement.click();
  }

  helpMe() {
    this.help = true;
    setTimeout(function(that) {
      that.help = false;
    }, 5000, this)
  }

  handleFileInput(files: FileList) {
    console.log('Handle file input');
    let fileToUpload = files.item(0);
    console.log(fileToUpload);
    this.isLive = false;

    let video: HTMLVideoElement = this.videoplayer;

    video.pause();
    this.drawImageToCanvas(fileToUpload);

    this._interactionService.sendImage(fileToUpload);
  }

  drawImageToCanvas(file) {
    console.log('Draw');
    let image = this.previewImage.nativeElement;
    console.log(image);
    let reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = function(th) {
        let that = th;
        return function(evt) {
          console.log('EVENT!!!!');
          if( evt.target.readyState == FileReader.DONE) {
            console.log('DONE!!!!');
    	    	image.src = evt.target.result;

    				//that.context.drawImage(image,0,0);
            that.fileInput.nativeElement.value = '';
    			}
        }
      }(this);

  }

}
