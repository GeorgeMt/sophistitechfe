import { Reference } from './reference';

export interface Product {
    id: number
    name: string
    gender: string
    labels: string[]
    imageURL: string
    url: string
    toggled?: boolean
    references: Reference[]

}
